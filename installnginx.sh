#!/bin/bash
#title          :installnginx
#description    :This script will install nginx from source
#author         :Talib Guyani <talibguyani@gmail.com>
#version        :0.1
#usage          :bash installnginx.sh
#notes          :tested on Debian8
#bash_version   :4.3.42(1)-release
#==============================================================================

echo "updating packages"
apt-get update -y

echo "installing build essentials and nginx dependencies"
sudo apt-get install build-essential libpcre3 libpcre3-dev libpcrecpp0 libssl-dev zlib1g-dev -y

echo "change to /opt dir, store nginx source here"
cd /opt/

echo "getting nginx source"
sudo wget http://nginx.org/download/nginx-1.11.3.tar.gz

echo "unzip nginx source"
sudo tar -xvzf nginx-1.11.3.tar.gz

echo "change to nginx dir"
cd nginx-1.11.3

echo "configure nginx with pcre and ssl module"
./configure --sbin-path=/usr/bin/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-debug --with-pcre --with-http_ssl_module

echo "make"
sudo make
sudo make install


echo "starting nginx"
nginx

echo "nginx installed"

echo "Daemon - /usr/bin/nginx"
echo "Conf - /ect/nginx/nginx.conf"
echo "Error log - /var/log/nginx/error.log"
echo "access log - /var/log/nginx/access.log"
